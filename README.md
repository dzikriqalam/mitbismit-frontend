
# **mitbismit frontend repository**
Ini merupakan repository khusus frontend untuk salah satu program kerja bismit yakni _Portofolio_

<br>
<hr>


# Untuk memulai project
```
1. git clone https://gitlab.com/pti-bemcsui/mitbismit-frontend.git  atau menggunakan ssh masing - masing
2. npm install 
3. npm start atau yarn start 
```

# node modules yang wajib digunakan yakni
1. react-router
2. styled - component
Pastikan anda mengerti kedua hal tersebut, jika belum pelajarilah dengan menonton video dibawah ini : 
router : https://www.youtube.com/watch?v=Law7wfdg_ls
styled-component : https://www.youtube.com/watch?v=c5-Vex3ufFU&t=625s

<br>
<br>
<hr>

# node tambahan yang digunakan 
1. isi disini 
2. ................
3. ................
Mohon disi agar coding style bisa sama satu dengan yang lainnya 

<br>
<br>
<hr>

# Untuk dapatkan update dari main ke branch masing - masing

```
1. git pull original main
```

**Note : dilakukan di posisi git branch masing - masing**

<br>
<br>
<hr>

# Untuk push ke branch masing - masing 

```
1.git add .
2.git commit -m'Fitur teraru'
2.git push (langsung push ke branch masing - masing)

                    Atau
2. git push --set-upstream origin <nama  branch>

```
_Note : Bisa dilakuakan apabila berada pada branch masing-masing_
<br>
<br>
<hr>

# Untuk push ke master dari branch masing - masing
```
1. git checkout master
2. git pull               # to update the latest  master state
3. git merge develop      # to merge branch to master
3. git push origin master # push current HEAD to master
```
<br>
<br>
<hr>

# Gunakan branch masing - masing ya gais.... 
```
format : git branch [nama branch]

contoh : git branch budi

         git branch

         git checkout budi
```

<br>
<br>
<hr>

# Gunakan `yarn start` OR  `npm start` untuk memulai

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
